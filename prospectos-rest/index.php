<?php

    // Incluimos archivo de conexion y cabeceras
    include('conexion_db.php');
    $json = file_get_contents('php://input'); // Recibe el JSON desde angular
 
    $params = json_decode($json); // Decodifica el JSON y lo guarda en una variable

    // Separamos los parametros en dos variables
    $usuario = $params -> username;
    $password = $params -> password;
    
    session_start();
    $pass_hashed = hash("sha512", $password);
    $query = $conexion -> prepare("SELECT * FROM usuarios WHERE username = :usuario AND password = :pass_hashed");
    $query -> bindParam(":usuario", $usuario); 
    $query -> bindParam(":pass_hashed", $pass_hashed);
    $query -> execute();
    $user = $query -> fetch(PDO::FETCH_ASSOC);
    if($user){
        echo json_encode(true);
    }else{
        echo json_encode("Usuario o contraseña incorrectos");
    }
    
?>