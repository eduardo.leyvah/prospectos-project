<?php

    // Credenciales de DB
    define('DB_HOST','localhost');
    define('DB_USER','prospectosdb');
    define('DB_PASS','1Prospectos-');
    define('DB_NAME','seguimiento_prospectos');

    // Cabeceras para las peticiones
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: access");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    // Establecemos la conexión.
    try{
        // Ejecutamos las variables y aplicamos UTF8
        $conexion = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=UTF8",DB_USER, DB_PASS);
    }
    // Atrapamos y mostramos los errores
    catch (PDOException $e){
        exit("Error de conexión: " . $e->getMessage());
    }

?>