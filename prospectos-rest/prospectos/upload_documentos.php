<?php
    // Incluimos archivo de conexion y cabeceras
    include('../conexion_db.php');

    $id_usuario = $_POST['id']; // Recibimos id del prospecto de la peticion POST

    //Preparamos y ejecutamos la consulta para traer el RFC del prospecto
    $query = $conexion -> prepare("SELECT p.rfc FROM `documentos` d JOIN prospectos p ON p.id = d.id_prospecto_fk AND d.id_prospecto_fk = :id");
    $query -> bindParam(":id", $id_usuario); 
    $query -> execute();
    $rfc = $query->fetch(PDO::FETCH_COLUMN);
    
    //Preparamos y ejecutamos la consulta para traer los Nombres de los documentos del prospecto
    $query = $conexion -> prepare("SELECT d.nombre_documento FROM `documentos` d JOIN prospectos p ON p.id = d.id_prospecto_fk AND d.id_prospecto_fk = :id");
    $query -> bindParam(":id", $id_usuario); 
    $query -> execute();
    $nombres = $query->fetchAll(PDO::FETCH_COLUMN);
    
    // Creamos el path donde se guardaran los archivos y asignamos el permiso para que se puede escribir dentro del directorio
    $uploadFolder =  "../documentos/$rfc/";
    chmod($uploadFolder, 0777);
    
    $files = $_FILES["file"]["name"]; // Recibimos el FromData que fue enviado en la peticion POST

    // Recorremos el arreglo de archivos para renombrarlo y subirlo al directorio seleccionado 
    for  ($i =  0; $i < count($files); $i++)  {
        $filename=$files[$i];
        $ext =  end(explode(".", $filename));
        $original = pathinfo($filename, PATHINFO_FILENAME);
        $fileurl = $nombres[$i];
        move_uploaded_file($_FILES["file"]["tmp_name"][$i], $uploadFolder . $fileurl);
    }
?>