<?php
    // Incluimos archivo de conexion y cabeceras
    include('../conexion_db.php');

    $json = file_get_contents('php://input'); // Recibe el JSON desde angular
 
    $params = json_decode($json); // Decodifica el JSON y lo guarda en una variable

    // Separamos los parametros en dos variables
    $nombre     = $params -> nombre_documento;
    $id_usuario = $params -> id_usuario;
    $path       = $params -> path;

    $query = $conexion -> prepare("SELECT rfc FROM `prospectos` WHERE id=:id");
    $query -> bindParam(":id", $id_usuario); 
    $query -> execute();
    $rfc = $query->fetch(PDO::FETCH_COLUMN);

    $path = "documentos/$rfc/";

    // Preparamos y ejecutamos consulta para insertar un nuevo prospecto
    $query = $conexion -> prepare("INSERT INTO documentos (nombre_documento, path_doc, id_prospecto_fk) VALUES (?, ?, ?)");

    // Enlazamos los parametros con las variables que anteriormente creamos
    $query -> bindParam(1, $nombre);
    $query -> bindParam(2, $path);
    $query -> bindParam(3, $id_usuario);

    // Ejecutamos la consulta y verificamos que el registro fue creado
    if($query -> execute()){
        echo json_encode("Documentos registrados correctamente");
    }else{
        echo json_encode("Error al insertar datos");
    }
    
?>