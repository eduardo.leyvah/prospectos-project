<?php

    // Incluimos archivo de conexion y cabeceras
    include('../conexion_db.php');

    $json = file_get_contents('php://input'); // Recibe el JSON desde angular
 
    $params = json_decode($json); // Decodifica el JSON y lo guarda en una variable

    // Separamos los parametros en dos variables
    $id = $params -> id;
    
    // Preparamos y ejecutamos consulta para obtener los prospectos que se encuentran en la DB
    $query = $conexion -> prepare("SELECT * FROM documentos WHERE id_prospecto_fk = :id");
    $query -> bindParam(":id", $id); 
    $query -> execute();
    $documentos = $query->fetchAll(PDO::FETCH_ASSOC);

    // Enviamos los prospectos obtenidos en formato JSON
    if($documentos){
        echo json_encode($documentos);
    }else{
        echo json_encode("Error en la consulta");
    }
    
?>