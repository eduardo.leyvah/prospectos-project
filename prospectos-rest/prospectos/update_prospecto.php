<?php

// Incluimos archivo de conexion y cabeceras
include('../conexion_db.php');

$json = file_get_contents('php://input'); // Recibe el JSON desde angular

$params = json_decode($json); // Decodifica el JSON y lo guarda en una variable

// Separamos los parametros en variables
$id_usuario = $params -> id_usuario;
$id_estatus_fk = $params -> id_estatus_fk;
$observacion = $params -> observacion;

// Verificamos si el prospecto fue autorizado o rechazado para poder actualizar el registro
if($id_estatus_fk == 2){
    
    $query = $conexion -> prepare("UPDATE `prospectos` SET `id_estatus_fk` = :id_estatus_fk WHERE `id` = :id");
    $query -> bindParam(":id_estatus_fk", $id_estatus_fk);
    $query -> bindParam(":id", $id_usuario); 
    
    if($query -> execute()){
        echo json_encode("Prospecto autorizado exitosamente");
    }else{
        echo json_encode("Error al autorizar el registro");
    }
    
}else{

    $query = $conexion -> prepare("UPDATE `prospectos` SET `id_estatus_fk` = :id_estatus_fk, `observacion` = :observacion WHERE `id` = :id");
    $query -> bindParam(":observacion", $observacion);
    $query -> bindParam(":id_estatus_fk", $id_estatus_fk);
    $query -> bindParam(":id", $id_usuario); 
    
    if($query -> execute()){
        echo json_encode("Prospecto rechazado exitosamente");
    }else{
        echo json_encode("Error al rechazar el registro");
    }

}

