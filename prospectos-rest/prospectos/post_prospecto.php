<?php
    // Incluimos archivo de conexion y cabeceras
    include('../conexion_db.php');

    $json = file_get_contents('php://input'); // Recibe el JSON desde angular
 
    $params = json_decode($json); // Decodifica el JSON y lo guarda en una variable

    // Separamos los parametros en dos variables
    $nombre           = $params -> nombre;
    $primer_apellido  = $params -> primerAp;
    $segundo_apellido = $params -> segundoAp;
    $calle            = $params -> calle;
    $numero           = $params -> numero;
    $colonia          = $params -> colonia;
    $codigo_postal    = $params -> cp;
    $telefono         = $params -> telefono;
    $rfc              = $params -> rfc;
    $observacion      = $params -> observacion;
    $id_estatus_fk    = $params -> id_estatus_fk;

    // Especificamos el path y el nombre del directorio para almacenar los archivos del prospecto
    $carpeta = "../documentos/$rfc";

    // Preparamos y ejecutamos consulta para insertar un nuevo prospecto
    $query = $conexion -> prepare("INSERT INTO prospectos (nombre, primer_apellido, segundo_apellido, calle, numero, colonia, codigo_postal, telefono, rfc, id_estatus_fk) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    // Enlazamos los parametros con las variables que anteriormente creamos
    $query -> bindParam(1, $nombre);
    $query -> bindParam(2, $primer_apellido);
    $query -> bindParam(3, $segundo_apellido);
    $query -> bindParam(4, $calle);
    $query -> bindParam(5, $numero);
    $query -> bindParam(6, $colonia);
    $query -> bindParam(7, $codigo_postal);
    $query -> bindParam(8, $telefono);
    $query -> bindParam(9, $rfc);
    $query -> bindParam(10, $id_estatus_fk);

    // Ejecutamos la consulta y verificamos que el registro fue creado
    if($query -> execute()){
        $id = $conexion->lastInsertId();
        echo json_encode($id, JSON_FORCE_OBJECT);
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
    }else{
        echo json_encode("Error al insertar datos");
    }
    
?>