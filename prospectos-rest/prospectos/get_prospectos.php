<?php

    // Incluimos archivo de conexion y cabeceras
    include('../conexion_db.php');
    
    // Preparamos y ejecutamos consulta para obtener los prospectos que se encuentran en la DB
    $query = $conexion -> prepare("SELECT p.id, p.nombre, p.primer_apellido, p.segundo_apellido, e.nombre_estatus FROM `prospectos` p JOIN `estatus` e ON e.id = p.id_estatus_fk");
    $query -> execute();
    $prospectos = $query->fetchAll(PDO::FETCH_ASSOC);

    // Enviamos los prospectos obtenidos en formato JSON
    if($prospectos){
        echo json_encode($prospectos);
    }else{
        echo json_encode("Error en la consulta");
    }
    
?>