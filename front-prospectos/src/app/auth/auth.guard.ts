import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { LoginService } from '../services/login.service';

// Clase que verificará si un usuario está logueado en el sistema para acceder a las demás rutas

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: LoginService, private router: Router) { }

    canActivate() {
        // Si el usuario no está logueado, será redirigido a la página de Login
        if (!this.authService.isLogged()) {
            console.error('No estás logueado');
            this.router.navigate(['/']);
            return false;
        }
        return true;
    }
}