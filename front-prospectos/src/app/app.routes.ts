import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { AgregarProspectoComponent } from './pages/agregar-prospecto/agregar-prospecto.component';

import { HomeEvaluadorComponent } from './pages/home-evaluador/home-evaluador.component';
import { HomePromotorComponent } from './pages/home-promotor/home-promotor.component';
import { LoginComponent } from './pages/login/login.component';
import { ProspectoComponent } from './pages/ver-prospecto/prospecto.component';

export const APP_ROUTES: Routes = [
    { path: 'login',  component: LoginComponent },
    { path: 'home-promotor',  component: HomePromotorComponent, canActivate: [AuthGuard]},
    { path: 'home-evaluador',  component: HomeEvaluadorComponent, canActivate: [AuthGuard] },
    { path: 'home-promotor/prospecto/:id',  component: ProspectoComponent , canActivate: [AuthGuard] },
    { path: 'home-evaluador/prospecto/:id',  component: ProspectoComponent , canActivate: [AuthGuard] },
    { path: 'home-promotor/agregar-prospecto',  component: AgregarProspectoComponent , canActivate: [AuthGuard] },
    { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const APP_ROUTING =  RouterModule.forRoot(APP_ROUTES);