import { Component, OnInit } from '@angular/core';
import { ProspectosService } from 'src/app/services/prospectos.service';

@Component({
  selector: 'app-table-prospectos',
  templateUrl: './table-prospectos.component.html',
  styles: [
  ]
})
export class TableProspectosComponent implements OnInit {

  prospectos = [];
  

  constructor( private prospectoService: ProspectosService) { 
    
  }

  ngOnInit(): void {
    this.prospectos = this.prospectoService.getProspectos();
  }


}
