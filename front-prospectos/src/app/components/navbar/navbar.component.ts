import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent{

  user = localStorage.getItem('user');

  isEvaluador: boolean = false;

  constructor( private loginService: LoginService,
    private router: Router ) { 

      if (this.user == 'admin'){
        this.isEvaluador = true;
      }

  }

  cerrarSesion(){

    this.router.navigateByUrl('login');
    this.loginService.logout();

  }

}
