import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomePromotorComponent } from './pages/home-promotor/home-promotor.component';
import { APP_ROUTING } from './app.routes';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { HomeEvaluadorComponent } from './pages/home-evaluador/home-evaluador.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProspectoComponent } from './pages/ver-prospecto/prospecto.component';
import { AgregarProspectoComponent } from './pages/agregar-prospecto/agregar-prospecto.component';
import { TableProspectosComponent } from './components/table-prospectos/table-prospectos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePromotorComponent,
    LoginComponent,
    HomeEvaluadorComponent,
    NavbarComponent,
    ProspectoComponent,
    AgregarProspectoComponent,
    TableProspectosComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
