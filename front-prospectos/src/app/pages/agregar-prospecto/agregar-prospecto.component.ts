import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Prospecto } from 'src/app/models/prospecto.model';
import { ProspectosService } from 'src/app/services/prospectos.service';
import { Documento } from 'src/app/models/documento.model';

@Component({
  selector: 'app-agregar-prospecto',
  templateUrl: './agregar-prospecto.component.html',
  styles: [
  ]
})
export class AgregarProspectoComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  files:string  []  =  [];
  newFormData: FormData;
  extension:string;
  
  datosProspecto: Prospecto = {
    nombre : null,
    primerAp : null,
    segundoAp : null,
    calle : null,
    numero : null,
    colonia : null,
    cp : null,
    telefono : null,
    rfc : null,
    observacion : null,
    id_estatus_fk : 1
  };

  datosDocumento: Documento = {
    nombre_documento : null,
    id_usuario : null,
    path_doc : null
  }

  constructor( private formBuilder: FormBuilder,
               private router: Router,
               private prospectosService: ProspectosService) {
      this.crearFormulario();
  }

  ngOnInit(): void{
  }
  
  crearFormulario() {

    this.form = this.formBuilder.group({
      nombre          : ['', [ Validators.required, Validators.minLength(3) ]  ],
      primer_apellido : ['', [Validators.required, Validators.minLength(3)] ],
      segundo_apellido: [''],
      calle           : ['', [ Validators.required, Validators.minLength(3) ] ],
      numero          : ['', [ Validators.required, Validators.minLength(3) ] ],
      colonia         : ['', [ Validators.required, Validators.minLength(3) ] ],
      codigo_postal   : ['', [ Validators.required, Validators.minLength(5), Validators.maxLength(5) ] ],
      telefono        : ['', [ Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
      rfc             : ['', [ Validators.required, Validators.pattern('^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([A-Z0-9]{3})$')] ],
      documentos: this.formBuilder.array( [] )

    });

  }

  get documentos(): FormArray{
    return this.form.get('documentos') as FormArray;
  }

  get formData() { return this.form.controls; }

  agregarDocumento() {
    this.documentos.push( this.formBuilder.group({
      fileName: ['', [Validators.required]],
      file:  ['', [Validators.required]]
    }));
  }
  
  borrarDocumento(i: number) {
    this.documentos.removeAt(i);
  }

  onFileChange(event)  {
    for  (var i =  0; i <  event.target.files.length; i++)  { 
        this.files.push(event.target.files[i]);
    }
  }

  onSubmit() {

    this.submitted = true;
    const formData =  new  FormData();


    for  (var i =  0; i <  this.files.length; i++)  {  
      formData.append("file[]",  this.files[i]);
      this.extension = this.documentos.value[i]['file'].split('.').pop();
      console.log(this.datosDocumento.nombre_documento = this.documentos.value[i]['fileName'].concat('.',this.extension)); 
    }
    
    this.datosProspecto['nombre'] = this.formData.nombre.value;
    this.datosProspecto['primerAp'] = this.formData.primer_apellido.value;
    this.datosProspecto['segundoAp'] = this.formData.segundo_apellido.value;
    this.datosProspecto['calle'] = this.formData.calle.value;
    this.datosProspecto['numero'] = this.formData.numero.value;
    this.datosProspecto['colonia'] = this.formData.colonia.value;
    this.datosProspecto['cp'] = this.formData.codigo_postal.value;
    this.datosProspecto['telefono'] = this.formData.telefono.value;
    this.datosProspecto['rfc'] = this.formData.rfc.value;  
    
    if( (!this.formData.nombre.valid) || (!this.formData.primer_apellido.valid) ||
        (!this.formData.calle.valid) || (!this.formData.numero.valid) || 
        (!this.formData.colonia.valid) || (!this.formData.codigo_postal.valid) || 
        (!this.formData.telefono.valid) || (!this.formData.rfc.valid) || this.files.length == 0 ){
      return;
    }

    for (let index = 0; index < this.documentos.value.length; index++) {
      
      if(this.documentos.value[index]['fileName'] == ""){
        return;
      }
      
    }

    console.log(this.datosProspecto);

    this.prospectosService.agregarProspecto(this.datosProspecto).subscribe(responseAgregar => {

      formData.append("id",  responseAgregar.toString());

      for (let index = 0; index < this.documentos.value.length; index++) {
        
        this.extension = this.documentos.value[index]['file'].split('.').pop();
        
        this.datosDocumento.nombre_documento = this.documentos.value[index]['fileName'].concat('.',this.extension);
        
        this.datosDocumento.id_usuario = parseInt(responseAgregar.toString());
        
        // this.prospectosService.registrarDocumento( this.datosDocumento );
        this.prospectosService.registrarDocumento( this.datosDocumento ).subscribe( responseRegistrar =>
          this.prospectosService.subirDocumentos(formData).subscribe(
            responseUpload => {
              Swal.fire({
                  icon: 'success',
                  text: 'Prospecto registrado exitosamente',
                  showConfirmButton: false,
                  timer: 2000
                }).then((result) => {
                  this.router.navigate(['home-promotor']);
                });
            }
          )
        );
        
      }

    });

  }

  salir(){
    Swal.fire({
      title: 'Advertencia',
      icon: 'warning',
      text: 'Si sale de se perderá toda la información capturada',
      showConfirmButton: true
    }).then((result) => {
      this.router.navigate(['home-promotor']);
    });
  }

}
