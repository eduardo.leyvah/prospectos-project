import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { Documento } from 'src/app/models/documento.model';
import { Prospecto } from 'src/app/models/prospecto.model';

import { ProspectosService } from 'src/app/services/prospectos.service';


@Component({
  selector: 'app-prospecto',
  templateUrl: './prospecto.component.html',
  styles: [
  ]
})
export class ProspectoComponent implements OnInit {

  datos = {
    id: 0
  };

  datosProspecto = {
    id_usuario: 0,
    id_estatus_fk: 0,
    observacion: ""
  }

  id : number;

  user = localStorage.getItem('user');

  isEvaluador: boolean = false;
  buttonsEvaluador: boolean = false;
  btnEvaluar: boolean = true;
  isRechazado: boolean = false;
  observaciones: boolean = false;
  submitted = false;
  
  prospecto: Prospecto[] = [];
  documentos: Documento[] = [];

  observacionesProspecto: string;
  form: FormGroup;

 urlDocumentos = [];

 url = 'http://192.168.10.11/prospectos-rest/';

  constructor(private rutaActiva: ActivatedRoute,
              private prospectoService: ProspectosService,
              private formBuilder: FormBuilder,
              private router: Router) { 
                
    if (this.user == 'admin'){
      this.isEvaluador = true;
    }
    
    // Obtener id del prospecto 
    this.rutaActiva.paramMap.subscribe( params => {
      
      this.datos['id'] = parseInt(params.get('id'));
      this.id = parseInt(params.get('id'));
    }); 
    

  }

  ngOnInit(): void {
    this.getDatosProspecto(this.datos);
    this.crearTextObservacion();
  }

  getDatosProspecto( datos: any ){

    // Llamar consulta para obtener los datos del prospecto
    this.prospectoService.getProspecto(datos).subscribe( (data) => {
      for (let index = 0; index < data.length; index++) {
        if (data[index].segundoAp == null) {
          data[index].segundoAp = "";
        }

        if(data[index].id_estatus_fk == 3){
          this.observaciones = true;
          
        }

        if( this.isEvaluador == true &&  data[index].id_estatus_fk == 1){
          this.buttonsEvaluador = true;
        }else{
          this.buttonsEvaluador = false;
        }
    }
      this.prospecto = data;
    });

    // Llamar consulta para obtener los datos de los documentos del prospecto y concatenar la URL
    this.prospectoService.getDocumentosProspecto(datos).subscribe( (data) => {
      for (let index = 0; index < data.length; index++) {
        this.urlDocumentos.push(this.url.concat(data[index].path_doc, data[index].nombre_documento));
      }
      this.documentos = data;
    });

    // Llamar consulta para obtener las observaciones del prospecto que fue rechazado
    this.prospectoService.getObservacionesProspecto(datos).subscribe( (data) => {
      data.forEach(element => {
        this.observacionesProspecto = element.observacion;
      });
    });

  }

  get formData() { return this.form.controls; }

  crearTextObservacion() {

    this.form = this.formBuilder.group({
      observacion : ['', [ Validators.required, Validators.minLength(10) ]  ]
    });

  }

  autorizar( id_estatus_fk : number ){
    this.datosProspecto['id_usuario'] = this.id;
    this.datosProspecto['id_estatus_fk'] = id_estatus_fk;
    console.log(this.datosProspecto);
    this.prospectoService.updateProspecto(this.datosProspecto).subscribe(response =>{
      Swal.fire({
        icon: 'success',
        text: response.toString(),
        showConfirmButton: false,
        timer: 2000
      }).then((result) => {
        this.router.navigate(['home-promotor']);
      });
    });
  }

  rechazar( id_estatus_fk : number ){
    this.submitted = true;

    this.datosProspecto['id_usuario'] = this.id;
    this.datosProspecto['id_estatus_fk'] = id_estatus_fk;
    this.datosProspecto['observacion'] = this.formData.observacion.value;

    if(this.formData.observacion.value == ""){return;}

    console.log(this.datosProspecto);
    this.prospectoService.updateProspecto(this.datosProspecto).subscribe(response =>{
      Swal.fire({
        icon: 'success',
        text: response.toString(),
        showConfirmButton: false,
        timer: 2000
      }).then((result) => {
        this.router.navigate(['home-promotor']);
      });
    });

  }

  cancelar(){
    this.isRechazado = false;
    this.datosProspecto['observacion'] = "";
  }

}
