import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario.model';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  datosUsuario: Usuario = {
    username: null,
    password: null
  }


  constructor( private formBuilder: FormBuilder,
               private login: LoginService ,
               private router: Router) {
    
  }
  
  ngOnInit(): void {

    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    
  }

  get formData() { return this.form.controls; }

  onSubmit() {

    this.datosUsuario['username'] = this.formData.username.value;
    this.datosUsuario['password'] = this.formData.password.value;

    this.submitted = true;

    if (this.formData.username.value == "" || this.formData.password.value == "") {
      return;
    }else{
      this.login.loginUsuario(this.datosUsuario).subscribe( datos => {
        if( datos == true ){
          localStorage.setItem('user', this.datosUsuario.username);
          Swal.fire({
            title: 'Sesión iniciada correctamente',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false
          });
          if( localStorage.getItem('user') == 'promotor'){
            this.router.navigateByUrl('home-promotor');
          }else{
            this.router.navigateByUrl('home-evaluador');
          }
          
        }else{
          Swal.fire({
            title: datos,
            icon: 'error',
            timer: 1500,
            showConfirmButton: false
          });
        }
        
      });
    }

  }

}
