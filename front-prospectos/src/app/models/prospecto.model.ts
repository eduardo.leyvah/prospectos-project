export class Prospecto{

    nombre : string;
    primerAp : string;
    segundoAp : string;
    calle : string;
    numero : number;
    colonia : string;
    cp : number;
    telefono : string;
    rfc : string;
    observacion : string;
    id_estatus_fk : number;

    constructor( nombre: string, primer_apellido: string, segundo_apellido: string, 
        calle: string, numero: number, colonia: string, codigo_postal: number, 
        telefono: string, rfc: string, id_estatus_fk: number, ){
        
        this.nombre = nombre;
        this.primerAp = primer_apellido ;
        this.segundoAp = segundo_apellido;
        this.calle = calle;
        this.numero = numero;
        this.colonia = colonia;
        this.cp = codigo_postal;
        this.telefono = telefono;
        this.rfc = rfc;
        this.id_estatus_fk = id_estatus_fk;
        
    }
    
}