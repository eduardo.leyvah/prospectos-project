import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Prospecto } from '../models/prospecto.model';

import { Observable } from 'rxjs';
import { Documento } from '../models/documento.model';

@Injectable({
  providedIn: 'root'
})

// Creamos un servicio para manejar las peticiones REST de Promotores
export class ProspectosService {

  URL = "http://192.168.10.11/prospectos-rest/prospectos/";
  
  prospectos: Prospecto[] = [];

  constructor( private http: HttpClient ) { }

  // Obtiene los datos de los prospectos que se encuentran en la DB
  getProspectos() {
   
    this.http.get(`${this.URL}get_prospectos.php`).subscribe(
      (data: Prospecto) => this.prospectos.push(data)
    );

    return this.prospectos;

  }

  getProspecto( datos: any ) : Observable<Prospecto[]>{

    return this.http.post<Prospecto[]>(`${this.URL}get_prospecto_by_id.php`, JSON.stringify(datos));

  }

  getDocumentosProspecto( datos: any ) : Observable<Documento[]>{
    
    return this.http.post<Documento[]>(`${this.URL}get_documentos_prospecto.php`, JSON.stringify(datos));

  }

  getObservacionesProspecto( datos: any ) : Observable<Prospecto[]>{

    return this.http.post<Prospecto[]>(`${this.URL}get_observaciones_prospecto.php`, JSON.stringify(datos));

  }

  agregarProspecto ( datosProspecto : Prospecto ){

    return this.http.post(`${this.URL}post_prospecto.php`, JSON.stringify(datosProspecto));

  }

  registrarDocumento ( datosDocumentos : Documento ){
    return this.http.post(`${this.URL}post_documentos.php`, datosDocumentos);
  }

  subirDocumentos ( formData: FormData ){
    return this.http.post(`${this.URL}upload_documentos.php`, formData);
  }

  updateProspecto ( datos: any ){
    return this.http.post(`${this.URL}update_prospecto.php`, JSON.stringify(datos));
  }


}
