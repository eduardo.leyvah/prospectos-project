import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Usuario } from '../models/usuario.model';


@Injectable({
  providedIn: 'root'
})

// Creamos un servicio para manejar las peticiones REST del login

export class LoginService {

  peticion: any;

  URL = "http://192.168.10.11/prospectos-rest/";

  constructor( private http: HttpClient ) { }

  loginUsuario( datosUsuario: Usuario ) {

    
    this.peticion = this.http.post(`${this.URL}index.php`, JSON.stringify(datosUsuario));

    return this.peticion;
    

  }

  isLogged(){
    if(localStorage.getItem('user')){
      return true;
    }
    return false;
  }

  logout(){
    localStorage.clear();
  }


}
